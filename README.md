# The Case File of Detective Martin Poole

This repository contains the session notes from a modern day Call of Cthulhu
campaign, where the PCs are all agents of a new Federal law enforcement agency
in the U.S. These notes are typed up from my hand-written notes that I take
during each session. The notes are in the voice of my character, Detective
Martin Poole, who has been seconded to this new agency from the U.S. Park Police
CID.

These notes are written in [org-mode](http://orgmode.org) for Emacs. This is
because org-mode strongly supports the list/bullet point style that I was
already using for my hand-written notes, plus it has a lot of nice, powerful
features for managing this sort of information. The fact that I can easily
export it to HTML for sharing with my group is also very nice.
